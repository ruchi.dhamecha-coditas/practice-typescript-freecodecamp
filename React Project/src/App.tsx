// import Component from "./starter/01-return";
// import Component from "./starter/02-props";
// import { Component } from "./starter/03-state";
// import Component  from "./starter/04-events";
import Component from "./starter/05-challenge";


function App() {
  return (
    <main>
      {/* <Component name="peter" id={123}>
        <h2>hello world</h2>
      </Component>
      <Component name="peter" id={123}/> */}

      {/* <Component type='basic' name="susan" email=anna@anna> 
      */}
     {/* <Component type='advanced' name="susan" email=anna@anna> 
      */}

      <Component/>

    </main>
  );
}
export default App;

// export default Component;
