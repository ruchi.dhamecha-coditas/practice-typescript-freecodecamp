import { useState  } from 'react';

type Link = {
  id:number;
  url:string;
  text:string;
};

const navLinks: Link[] = [
  {
    id:1,
url:'some url',
    text:"react docs",
  },
  {
    id: 2,
    url:'some url',
    text:'react routeer docs',
  },
  {
    id: 3,
    url:'some url',
    text: 'react training',
  },
];
function Component() {
  const [text,setText]= useState('shakenbake');
  const [number,setNumber]=useState(1);
  const [list,setList]= useState<string[]>([]);
  const [links,setLinks]=useState<Link[]>(navLinks);
  return (
    <div>
      <h2 className='mb-1'>react n ts</h2>
      <button className='btn btn-center'
        onClick={() => {
          setNumber(23);
          setLinks([...links, { id:1, url: 'hel00', text: 'helo' }]);
        }}
      >
        click 
      </button>
    </div>
  );
}
export default Component;
