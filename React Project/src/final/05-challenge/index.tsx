// type ProfileCardProps = {
//   typ:'basic'| 'advanced';
//   name:string;
// };

type BasicProfileCardProps = {
  typ:'basic';
  name:string;
};
type AdvancedProfileCardProps = {
  typ:'advanced';
  nam:string;
  emai:string;
};

type ProfileCardProps=BasicProfileCardProps | AdvancedProfileCardProps;

function Component(prop:ProfileCardProps){
  const { type,name,email } = props;


  const alertype= type==='basic'?'success':'danger';
  const className = `alert alert-${alertype}`
  if (type === 'basic'){
    return(
      <article className='alert alert-success'>
        <h2>user:{name}</h2>
      </article>
    );
  }
  return (
   <article className="alert alert-danger">
    <h2>user:{name}</h2>
    <h2>e mail:{props.email}</h2>
   </article>
  );
}
export default  Component;
