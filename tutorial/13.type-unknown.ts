//data type 
// unknown

let unknwnVal :unknown;
unknwnVal="hello";
unknwnVal=[1,3,4];
unknwnVal=32.435;


if(typeof unknwnVal==="number"){
    unknwnVal.toFixed(2);
}

function runSomeCode(){
    const random = Math.random();
    if (random< 0.5){
        throw new Error('there was error...');
    }
    else{
        throw 'some error';
    }
}
try{

    runSomeCode();
   
} catch(error){
    if (error instanceof Error){
        console.log(error.message);
    }
    else{
      console.log("error")  
    }
}

