interface Iperson{
    name:string;
    age:number;
    greet():void;
}

class Person implements Iperson{
    constructor(public name:string, public age:number){

    }
    greet():void{
        console.log(`hello my name is ${this.name} age: ${this.age}`);
    }
}
const hipster = new Person('gv dxjh',321);