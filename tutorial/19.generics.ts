function generateStringArray(length:number,value :string):string[]{
    let result: string[] = [];
    result = Array(length).fill(value);
    return result;
}

function createArray<T>(length:number, value: T): Array<T>{
    let result: T[] = [];
    result = Array(length).fill(value);
    return result;
}

let arrStr = createArray<string>(10,'hello');
let arrNum = createArray<Number>(10,6523);


console.log(arrNum);


