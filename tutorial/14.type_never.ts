//data type 
// never

// const someValues: never =0;


type Theme = 'light' | 'dark';

function checkTheme(theme: Theme): void{
    if(theme === 'light'){
        console.log('light theme');
        return;
    }
    if(theme === 'dark'){
        console.log('dark theme');
        return;
    }
}

enum Color {
    Red,
    Blue,
    Green
}

function getColorName(color){
    switch(color){
        case Color.Red:
            return 'Red';
        case Color.Blue:
            return 'Blue';
        case Color.Green:
            return "Green"
        default:
            let unexpectedColor: never = color;
            throw new Error(`unexpected color ${color}`)
    }
}

console.log(getColorName(Color.Red));
console.log(getColorName(Color.Blue));
console.log(getColorName(Color.Green));



