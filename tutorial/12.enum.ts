let someVal:any = 'this is a  string';

let strLength: number = (someVal as string).length;

type Bird = {
    name:string;
}

let birdStr='{"name":"Eagle"}';
let dogStr = '{"breed":"Poodle"}';

let birdObj = JSON.parse(birdStr);
let dogObj = JSON.parse(dogStr);

let bird = birdObj as Bird;
let dog = dogObj as Bird;

console.log(bird.name);
console.log(dog.name);

enum Status{
    Pending ="pending",
    Declined = "declined",
}
 type User={
    name:string;
    status : Status;
 };

 const statusVal = "pending";

 const user:User={
    name:'ax',
    status:statusVal as Status
 };
