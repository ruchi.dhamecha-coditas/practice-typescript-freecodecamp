type ValueType = string |number |boolean;

let value: ValueType;

const random = Math.random();
value = random <0.33 ? 'hello':random < 0.66? 123 :true;

function checkValue(value: ValueType):void{
    if(typeof value === "string"){
        console.log(value.toLocaleLowerCase());
        return;
    }
    if(typeof value === "number"){
        console.log(value.toLowerCase());
        return;
    }
    console.log(`boolean : ${value}`);
}

checkValue(value);


//exmaple 2

function printLength(str:string | null | undefined){
    if(str){
        console.log(str.length);
    }
    else{
        console.log('no string');
    }
}

printLength('hello');
printLength('');
printLength(null);
printLength();
printLength(undefined);