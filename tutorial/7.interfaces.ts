interface Book{
    readonly isbn :number;
    title :string,
    author:string,
    genre?:string,
    printAuthor():void;
    printTitle(message:string):string;
    // set method as interface in property 
    prints:(somval:number)=> number;
}

const deeepWork:Book= {
   isbn:123,
    title: 'deep work',
    author:'asbhj',
    genre:'sxba',
    printAuthor(){
        console.log(this.author);

    },
    printTitle(message) {
        return `${this.author} ${message}`
    },
    prints(somval) {
        return somval
    },
}

//deeepWork.isbn='same value';