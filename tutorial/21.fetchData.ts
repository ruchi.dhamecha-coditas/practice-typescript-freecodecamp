import { z } from 'zod';

const url = 'https://www.course-api.com/react-tours-projects';


const tourSchema =.object({
    id:z.string(),
    name:z.string(),
    info:z.string(),
    img:z.string(),
    price:z.string(),
    // something:z.number(),
})

type Tour = z.infer<typeof tourSchema>;

async function fetchdata(url:string):Promise<Tour[]>{
    try{
        const response = await fetch(url);
        if(!response.ok){
            throw new Error(`http err ${response.status}`);
            
        }
        const rawData = await response.json();
        const result = tourSchema.array().safeParse(rawData);

        if(!result.success){
            throw new console.error('invalid dara',result.error);
            
        }
    }catch(error){
        const errMsg= 
        error instanceof Error ? error.message : "error";
        console.log(errMsg);
        return [];
    }
}
const tours = await fetchdata(url);
tours.map((tour:any)=>{
    console.log(tour.name);
});