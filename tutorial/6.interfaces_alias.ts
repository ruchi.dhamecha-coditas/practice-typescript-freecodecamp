type StringOrNumber= string | number;

let value:StringOrNumber;
value ='hello';
value=123;

type Theme = 'light' | 'dark';

let theme :Theme;

theme = 'dark';
theme='light';

function setTheme(t:Theme){
    theme = t;
}

setTheme('dark')



///example

type Employee = {id:number;name:string;department:string};

type Manager = {id:number; name:string; employees:Employee[]}

type Staff = Employee |Manager;

function printStaffDetails(staff:Staff):void{
    if ('employees' in staff){
        console.log(
                `${staff.name} is manager in ${staff .employees.length} employees`
        );
    }
    else{
        console.log(
            `${staff.name} is an employee in the ${staff.department} department`
        );
    }
}

const a : Employee={id:1,name:'alice',department:'sales'};
const s : Employee={id:2,name:'alicesss',department:'hr'};

const b: Manager={id:1,name:'bobo',employees:[a,s]};


printStaffDetails(a);