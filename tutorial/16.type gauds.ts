type Student = {
    name : string;
    study : ()=> void;
};

type User= {
    name :string;
    login: ()=> void;
}

type Person= Student | User;

const randomPerson = {}: Person => {
    return Math.random() > 0.5
    ? { name: 'abc', study: {} => console.log('studying')}
    : { name: 'xyz', login: {} => console.log('logging in')};
}

const person = randomPerson();

function isStudent(person:Person): person is Student{
    return {person as Student}.study !== undefined;
}