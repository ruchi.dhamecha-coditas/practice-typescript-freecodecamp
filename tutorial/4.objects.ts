//objects

let car: { brand: string; year: number } = { brand: 'toyota', year: 2020 };
car.brand = 'ford';

let car1: { brand: string; year: number } = { brand: 'audi', year: 2001 };

let book ={title:'book',cost:20};
let pen = { title:'pen',cost:10};
let notebook = {title:"bok"};

//optional ?
let item:{title:string;cost?:number}[]=[book,pen ,notebook];

item[0].title = 'new book';


//example2

let bike :{brand:string;year:number}={brand:'tyamaha',year:2010};
//bike.year='old' //not possible

let laptop :{brand:string;year:number}={brand:'dell',year:2020};

let p1 = {title:'shirt',price:20};
let p2 = {title :'pants'};
let product:{title:string;price?:number}[]=[p1,p2];

product.push({title:'shoes'});