let a:string = "yes"
a= "no"
console.log(a)


// a= 20 // this cant be aasigned

let amount :number = 20;
amount = 12 - 2.1
//amount = "yes" //not possitble

let isOk:boolean=true
isOk=false

// if we dont declare data type it will consider data type of 1st declared one
let b = 6   //this became number
//b="no"



let greeting:string = "hello"
greeting.toUpperCase()

let age:number =25;
age =age+5;

let isAdult:boolean = age>18;
isAdult =!isAdult;
console.log(isAdult)


//union

let tax: number | string = 10;
tax = 100;
tax = '10';

//any
let notSure: any = 4;
notSure = 'maybe a string instead';
notSure = false; // okay, definitely a boolean


// Practical Application of Type Annotation
const books = ['2002', 'book', 'abcd'];

let foundBook: string | undefined;

for (let book of books) {
  if (book === '2002') {
    foundBook = book;
    foundBook = foundBook.toUpperCase();
    break;
  }
}

console.log(foundBook?.length);