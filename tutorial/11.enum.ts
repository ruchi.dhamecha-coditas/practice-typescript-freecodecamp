enum UserRole{
    Admin,
    Manager,
    Employee
}

type User={
    id:number;
    name:string;
    role: UserRole;
    contact:[string,string];
};

function createUser(user:User):User{
    return user;
}

const user:User=createUser({
    id:1,
    name:"bhj",
    role:UserRole.Admin,
    contact:['dsac@.','12324565'],
}); 