
function sayHi(name:string){
    console.log("hello")
}

function discount(price:number):any{
    const hasdiscount =true;
    if(hasdiscount){
        return 'discount applied'
    }
    return price * 0.9;
}
discount(200);

//so here we can return any bcoz string or number can be the return type

function add(number:any){
    let num:number =3;
    return number + num;
}
const result = add (3);
const someVal = result;

//someVal.abc() //err bcoz no such function available

//example

function price(price:number,discount?:number):number{
    return price - (discount || 0);
}

price(200,100);



// more than 3 4 parameters to be passed to the function

function sum(message:string,...numbers:number[]){
    const doubled = numbers.map((num)=>num*2);
    console.log(doubled);
}

let results = sum(`total`, 1,2,3,4,4,56);



//when we are not returning anything drom the fuciion

//void

function abc (b:string):void{
    console.log(b);
    //return "hello";  //error
}

abc('hello');

//typeof
function process(input: string | number){
    if (typeof input === 'number'){
    console.log(input *  2);

    }
}

//objects as a param in function
    function student(student:{id:number;name:string}):void{
        console.log(`welcome to course${student.name.toUpperCase()}`)
    }
    const newstudent={
    id:5,
    name:'anna'
};
student(newstudent);