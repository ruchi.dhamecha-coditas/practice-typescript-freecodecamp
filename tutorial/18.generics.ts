let arr1:string[]=['appple','banana','mango'];
let arr2:number[]=[1,2,3];
let arr3:boolean[]=[true,false,true];

let array1: Array<string> = ['apple','banna','mango'];

//================================

function genericFunc<T>(arg: T): T {
    return arg;
}

const someStringValue = genericFunc<string>('hello world');
const someNumberValue = genericFunc<number>(2);

interface GenericInterface<T>  {
    value: T;
    getValue:() => T;
}

const genericString: GenericInterface<string> = {
    value: 'hello ',
    getValue() {
        return this.value;
    },
}

async function someFunc(): Promise<number>{
    return 2344;
}

const result = someFunc();

