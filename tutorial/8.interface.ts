interface Person{
    name :string;
    getDetails():string;
}
interface DogOwner{
    dog :string;
    dogDetails():string;
}
interface Person{
    age:number;
}
const person:Person={
     name:'john',
    age :30,
    getDetails(){
        return `Name: ${this.name} Age: ${this.age}`
    },

};
interface Employee extends Person{
    employeeId:number;
}

const employee:Employee={
    name:"ruchi",
    age:28,
    employeeId:123,
    getDetails() {
        return `Name: ${this.name} Age: ${this.age}, employee ${this.employeeId}`
    },
}

console.log(employee.getDetails());