function pair<T,U>(param1: T,param2:U):[T,U]{
    return [param1,param2];
}

let result = pair(123,'hello');

function processValue<T extends string | number>(value:T):T{
    console.log(value);
    return value;
}

processValue('hello');
processValue(234);
processValue(true);